import h from "react-hyperscript";
import { useState } from "react";
import { useApi } from "../src/apiHelpers";
import {
  NewsletterSignupMsg,
  NewsletterSignupResponse,
} from "../pages/api/signup/[action]";
import { LabelBox, FormBox } from "./Layout";
import { Input } from "./Form";
import { Secondary } from "./Button";

export default (props: { tags?: string[] }) => {
  let [email, setEmail] = useState("");
  let [status, callNewsletterSignup] = useApi<
    NewsletterSignupMsg,
    NewsletterSignupResponse
  >([email]);

  let onSubmit = (e: React.FormEvent) => {
    e.preventDefault();
    callNewsletterSignup("/api/signup/newsletter", { email, tags: props.tags });
  };

  return h(FormBox, { onSubmit, gap: 16 }, [
    h(LabelBox, { gap: 8 }, [
      h(Input, {
        placeholder: "Your email",
        type: "email",
        value: email,
        onChange: (e) => setEmail(e.currentTarget.value),
      }),
    ]),
    h(Secondary, { type: "submit", status }, "Subscribe!"),
  ]);
};
