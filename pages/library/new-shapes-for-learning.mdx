---
title: "New Shapes for Learning Experiences"
date: '2021-06-30'
author: Brendan
toc: true
living: false
tags: [learning-design]
description: "Join us to experiment with new possibilities for collaborative learning."
topic: ""
---

import {LibraryLayout} from '../../components/Library'
export default ({children}) => <LibraryLayout {...metadata}>{children}</LibraryLayout>

There are many more containers for collaborative learning than currently exist. We'd like to invite you to create some of them together.

So far, we've seen incredible variety in the topics people are exploring in courses and clubs, and many approaches to high-level [course structure](https://hyperlink.academy/library/catalog-of-course-structures), but there's still a vast space yet to explore, and we'd like to encourage more radical experimentation!

We're thinking about things like modular workshops, self-paced materials, async communication, longer academic programs, and other things that can augment facilitators' toolkits for creating great learning experiences.

## New Shapes for Learning

Here are some things we're interested in experimenting with on Hyperlink. If you have ideas for any of these, or something we haven't touched on yet, please let us know!

### Modular Experiences

It can be tempting to pack a course full of all the fascinating stuff you'd like to possibly cover. But long courses can be challenging for participants to fit in their schedules, and people may be coming with different backgrounds and interest in different parts of a course.

Some courses might benefit from being split into a series of shorter sprints (perhaps 2-3 week cohorts, or even single sessions), linked as a clear sequence, but modular, so that learners could join one, some, or all.

### Larger Cohorts

There are limits on how big a cohort can be while still feeling like an intimate group. But it's also possible to structure cohorts that scale, for example by organizing smaller clusters or 1:1 pairings.

Building the demand in the first place is still a fundamental challenge, but if you think you have the audience to attract > 20 people to a cohort, it's worth considering how to support a larger group in a way that still delivers a great experiences for everyone.

### Self-Paced Cohorts

If you have a predefined project or sequence of materials for learners to work through, but want to let them complete it on a variable schedule, you could design a cohort to have a rolling start, flexible duration, or otherwise give people ways to complete it at their own pace.

This would likely include a mix of content / exercises for people to digest, plus ways to get feedback from the facilitators or other participants, also on a variable schedule. The tradeoff here would be less momentum and accountability, but greater flexibility for learners.

A related idea could be a "Bring Your Own Cohort" option, where learners assemble e.g. a group of friends or team of coworkers, and you set up a cohort to match their desired schedule.

### Async-Focused Cohorts

Related to the above — but rather than the overall schedule varying per-learner, you might want to stick to a consistent overall schedule, yet primarily rely on asynchronous interactions among the group.

Such a course or club would still be based around a shared experience and interaction with other participants, with explicit structure to keep learners on track, but you'd focus on sharing work and exchanging feedback outside of live meetings.

To balance this, you could perhaps combine this with live onboarding (1:1 or in a group) at the start of the course, and/or optional live meetings during the cohort.

### Intensive Courses

For certain types of subjects and learner goals, we think there's space for more intensive and ambitious courses.

These would likely be on the longer side, perhaps semester-length, and/or with multiple meetings each week. And they'd likely hit a much higher price point than most courses currently on Hyperlink, probably in the thousands of dollars.

Such courses will take proportionally more effort all around — for creators to market and facilitate; for learners to show up and doing the work — but can be a great way for participants to complete a major project or transformation.

### Academic Programs

Building on the above: what would a from-scratch academic program for a niche field look like?

Beyond even a single intensive course, we're imagining some interesting possibilities for e.g. a one-year cohort focused on intensive skill-building.

This could be an alternative to an MFA, for someone interested in art school, or a variation on programming boot camps, likely for learners who want to go deep on a particular area, but aren't into the full-time commitment and expense of an advanced degree.

### Research Groups / Residencies

Similar to how we've encouraged clubs and an alternative to courses, as a format for peer-driven exploration, what would it look like to create cohorts for people researching a topic together?

This might follow a similar format to clubs, with one or more facilitators, but perhaps have a more directed research focus, with each participant working on a thesis-like project and convening for regular review and critique.

It seems particularly well-suited for areas that may be nascent, highly interdisciplinary, or otherwise fall somewhere in the interstices of academia, for example "tools for thought" or "experimental AR/VR".

### Co-Created Cohorts

Some cohorts might benefit from multiple people collaborating to create and facilitate together, bringing together different viewpoints and specialties.

We actually support this now on Hyperlink — it's possible to add other maintainers to a course or club, and create a cohort with multiple facilitators, and a couple clubs have tried this so far. This is one that could be combined with anything else on this list, and might be particularly good for a longer or more intensive program.

## Three Examples

To kick things off, some specific examples of things we'd be into—

### Month-long daily creative challenges

Brendan has been exploring cohorts based around a series of creative challenges, for example poetic forms, or neighborhood photography, with a challenge each day + focus on async sharing and feedback. What else could work nicely for this kind of structure?

### Programming language creation intensive

Jared would love to see a serious (6-12 month?) cohort for people embarking on a serious programming project, perhaps each creating a new language. What other intensive skills-based learning structures would you like to see?

### Creative craft-swap residency

Celine has ideas for some kind of crafter collective, exploring a variety of projects together and sharing materials, perhaps swapping via mail. What kind of residency program would you create with your friends?

## Experiments in the Key of Meta

Right now we're planning the next version of Hyperlink's [Meta Course](https://hyperlink.academy/courses/the-meta-course/1), and we want to incorporate some of these things within it as meta experiments.

We're also planning to run the next Meta Course on Hyperspace — and we're particularly interested in how some of these learning structure possibilities could work in Hyperspace, and inform our future roadmap.

Stay tuned, and [let us know](mailto:contact@hyperlink.academy) if you might like to join!
